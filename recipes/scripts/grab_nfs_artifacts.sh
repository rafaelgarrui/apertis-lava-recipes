#!/bin/sh

set -e

cp ${ROOTDIR}/boot/vmlinuz-* ${ARTIFACTDIR}/vmlinuz
cp ${ROOTDIR}/boot/initrd.img-* ${ARTIFACTDIR}/initrd.img
if [ -d ${ROOTDIR}/boot/dtbs ] ; then
	mkdir -p ${ARTIFACTDIR}/dtbs
	cp -r ${ROOTDIR}/boot/dtbs/*/* ${ARTIFACTDIR}/dtbs/
fi
