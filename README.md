Image recipes for use in lava as infrastructure these include:
* NFS bootable images for flashing

Recipes for the images (seperate ospack & hwpack) can be found in the recipes
directory. Example tests can be found in the lava subdirectory for i.MX6
sabrelite (32 bit arm) and Renesas R-Car M3 ULCB (64 bit arm).
